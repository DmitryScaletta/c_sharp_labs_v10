﻿using System;

namespace lab1
{
	class Program
	{
		static void Main(string[] args)
		{
			const string DIV_BY_ZERO = "Error: division by zero";

			int x;
			Console.Write("Enter x: ");
			string input = Console.ReadLine();
			if (!Int32.TryParse(input, out x))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			// y1
			double den = Math.Pow(x, 3) + 3;
			if (den == 0) {
				Console.WriteLine(DIV_BY_ZERO);
			}
			else
			{
				double y1 = 18 * x + 3 / den;
				Console.WriteLine("y1 = " + y1);
			}

			// y2
			double y2 = 18 - x;
			Console.WriteLine("y2 = " + y2);

			// y3
			den = Math.Pow(x, 2) + 2;
			if (den == 0)
			{
				Console.WriteLine(DIV_BY_ZERO);
			}
			else
			{
				double y3 = 4 * x + 7 / den;
				Console.WriteLine("y3 = " + y3);
			}
		}
	}
}
