﻿using System;

namespace lab2_2
{
	class Program
	{
		static void Main(string[] args)
		{
			const string DIV_BY_ZERO = "Error: division by zero";

			int x;
			int y;
			Console.Write("Enter x, y: ");
			string[] input = Console.ReadLine().Split(' ');
			if (input.Length < 2 || 
				!Int32.TryParse(input[0], out x) || 
				!Int32.TryParse(input[1], out y))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			int  xy     = x * y;
			int  f      = 0;
			int  g      = 0;
			bool f_div0 = false;
			bool g_div0 = false;
			int  den;

			switch (xy)
			{
				case -4:
				case -3:
				case -2:
				case -1:
				case 0:
				{
					den = (int) (Math.Pow(y, 3) - Math.Pow(x, 2)) + 1;
					if (den == 0)
					{
						f_div0 = true;
					}
					else
					{
						f = 3 * x + (int) (Math.Pow(y, 2) / den);
					}
					g = (int) Math.Pow((Math.Pow(x, 2) - y), 2) - y;
				}
				break;

				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				{
					den = 1 + (int) Math.Sin(x);
					if (den == 0)
					{
						g_div0 = true;
					}
					else
					{
						g = (int) (200 / den);
					}
					f = (int) (Math.Pow(x, 2) * Math.Pow(y, 2)) + 10;
				}
				break;

				case 6:
				case 7:
				case 8:
				{
					f = (int) (100 / (1 + Math.Abs(Math.Pow(x, 2) - y)));
					g = x + (int) Math.Abs(3 * Math.PI - xy);
				}
				break;

				default:
				{
					Console.WriteLine("Values must be in interval [-4;8]");
					return;
				}
			}

			Console.WriteLine("f = " + ((f_div0) ? DIV_BY_ZERO : f.ToString()));
			Console.WriteLine("g = " + ((g_div0) ? DIV_BY_ZERO : g.ToString()));
		}
	}
}
