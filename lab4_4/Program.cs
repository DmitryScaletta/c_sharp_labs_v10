﻿using System;

namespace lab4_4
{
	class Program
	{
		static void fill_array(int[] a)
		{
			Random rand = new Random();
			for (int i = 0; i < a.Length; ++i)
			{
				a[i] = rand.Next(-99, 99);
			}
		}

		static void show_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write(a[i] + "\t");
			}
			Console.WriteLine();
		}

		static void shift_array(int[] a, int idx)
		{
			for (int i = idx; i < a.Length - 1; ++i)
			{
				a[i] = a[i + 1];
			}
		}

		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;

			int n;
			int p;
			Console.Write("Enter n, p: ");
			string[] input = Console.ReadLine().Split(' ');
			if (input.Length < 2 ||
				!Int32.TryParse(input[0], out n) ||
				!Int32.TryParse(input[1], out p))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			Console.WriteLine("4. Сжать массив, удалив из него все элементы, кратные числу P.Освободившиеся в конце массива элемента заполнить нулями.\n");

			int[] a = new int[n];
			fill_array(a);
			show_array(a);

			int count = 0;
			for (int i = 0; i < a.Length - count;)
			{
				if (a[i] % p == 0)
				{
					shift_array(a, i);
					++count;
				}
				else
				{
					++i;
				}
			}

			for (int i = a.Length - 1; count > 0; --i)
			{
				a[i] = 0;
				--count;
			}

			show_array(a);
		}
	}
}
