﻿using System;

namespace lab2_1
{
	class Program
	{
		static void Main(string[] args)
		{
			const string DIV_BY_ZERO = "Error: division by zero";

			double x;
			double y;
			Console.Write("Enter x, y: ");
			string[] input = Console.ReadLine().Split(' ');
			if (input.Length < 2 || 
				!Double.TryParse(input[0], out x) || 
				!Double.TryParse(input[1], out y))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			double xy     = x * y;
			double f      = 0;
			double g      = 0;
			bool   f_div0 = false;
			bool   g_div0 = false;
			double den;

			if (xy < 1)
			{
				den = Math.Pow(y, 3) - Math.Pow(x, 2) + 1;
				if (den == 0)
				{
					f_div0 = true;
				}
				else
				{
					f = 3 * x + Math.Pow(y, 2) / den;
				}
				g = Math.Pow((Math.Pow(x, 2) - y), 2) - y;
			}
			else if (xy <= 5)
			{
				f = Math.Pow(x, 2) * Math.Pow(y, 2) + 10;
				den = 1 + Math.Sin(x);
				if (den == 0)
				{
					g_div0 = true;
				}
				else
				{
					g = 200 / den;
				}
			}
			else
			{
				f = 100 / (1 + Math.Abs(Math.Pow(x, 2) - y));
				g = x + Math.Abs(3 * Math.PI - xy);
			}

			Console.WriteLine("f = " + ((f_div0) ? DIV_BY_ZERO : f.ToString()));
			Console.WriteLine("g = " + ((g_div0) ? DIV_BY_ZERO : g.ToString()));
		}
	}
}
