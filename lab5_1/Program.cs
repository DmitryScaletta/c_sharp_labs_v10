﻿using System;
using System.Linq;

namespace lab5_1
{
	class Program
	{
		public static readonly char[] CONSONANTS = { 'б', 'в', 'г', 'д', 'ж', 'з', 'й', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь' };

		static void Main(string[] args)
		{
			// 10. Определить, какой процент слов в тексте содержит удвоенную согласную.

			Console.OutputEncoding = System.Text.Encoding.UTF8;

			Console.Write("Enter string: ");
			string[] text = Console.ReadLine()
				.Replace('.', ' ')
				.Replace('!', ' ')
				.Replace('?', ' ')
				.Replace(',', ' ')
				.Split();

			int word_count         = 0;
			int double_vowes_count = 0;
			for (int i = 0; i < text.Length; ++i)
			{
				if (text[i].Length == 0) continue;

				char[] chars = text[i].ToLower().ToCharArray();

				for (int j = 0; j < chars.Length - 1; ++j)
				{
					if (chars[j] == chars[j + 1])
					{
						if (CONSONANTS.Contains(chars[j]))
						{
							++double_vowes_count;
							break;
						}
					}
				}
				++word_count;
			}

			double percent = double_vowes_count / (double)word_count * 100;

			Console.WriteLine("Percent of words with the double vowel: " + percent + "%");
		}
	}
}
