﻿using System;

namespace lab4_6
{
	class Program
	{
		static void enter_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write("Enter a[" + i + "]: ");
				string input = Console.ReadLine();
				while (!Int32.TryParse(input, out a[i]))
				{
					Console.WriteLine("Wrong input");
				}
			}
		}

		static void show_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write(a[i] + "\t");
			}
			Console.WriteLine();
		}

		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;

			int n;
			Console.Write("Enter n: ");
			string input = Console.ReadLine();
			if (!Int32.TryParse(input, out n))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			Console.WriteLine("6. Дан вектор A(n). Все элементы вектора, предшествующие первому наименьшему элементу умножить на 10, если элемент минимальный по величине встречается в последовательности более чем один раз. В противном случае вектор оставить без изменения.\n");

			int[] a = new int[n];
			enter_array(a);
			show_array(a);

			int min_v = a[0];
			int min_i = 0;

			for (int i = 0; i < a.Length; ++i)
			{
				if (a[i] < min_v)
				{
					min_v = a[i];
					min_i = i;
				}
			}

			int count = 0;
			for (int i = 0; i < a.Length; ++i)
			{
				if (a[i] == min_v) ++count;
			}

			if (count > 1)
			{
				for (int i = 0; i < a.Length; ++i)
				{
					if (a[i] == min_v) break;
					a[i] *= 10;
				}
			}

			show_array(a);
		}
	}
}
