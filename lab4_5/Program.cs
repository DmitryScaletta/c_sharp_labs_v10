﻿using System;

namespace lab4_5
{
	class Program
	{
		static void fill_array(int[] a)
		{
			Random rand = new Random();
			for (int i = 0; i < a.Length; ++i)
			{
				a[i] = rand.Next(-99, 99);
			}
		}

		static void show_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write(a[i] + "\t");
			}
			Console.WriteLine();
		}

		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;

			int n;
			Console.Write("Enter n: ");
			string input = Console.ReadLine();
			if (!Int32.TryParse(input, out n))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			Console.WriteLine("5. Если максимальный элемент находиться правее минимального, то найти сумму элементов, расположенных за максимальным.\n");

			int[] a = new int[n];
			fill_array(a);
			show_array(a);

			int max_v = a[0];
			int max_i = 0;
			int min_v = a[0];
			int min_i = 0;

			for (int i = 0; i < a.Length; ++i)
			{
				if (a[i] < min_v)
				{
					min_v = a[i];
					min_i = i;
				}
				if (a[i] > max_v)
				{
					max_v = a[i];
					max_i = i;
				}
			}

			if (max_i > min_i)
			{
				int sum = 0;
				for (int i = max_i + 1; i < a.Length; ++i)
				{
					sum += a[i];
				}
				Console.WriteLine("The sum of all elements after the max element: " + sum);
			}
			else
			{
				Console.WriteLine("The max element not on the right side of the min element");
			}

		}
	}
}
