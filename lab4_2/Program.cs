﻿using System;

namespace lab4_2
{
	class Program
	{
		static void enter_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write("Enter a[" + i + "]: ");
				string input = Console.ReadLine();
				while (!Int32.TryParse(input, out a[i]))
				{
					Console.WriteLine("Wrong input");
				}
			}
		}

		static void show_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write(a[i] + "\t");
			}
			Console.WriteLine();
		}

		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;

			int n;
			int c;
			int d;
			Console.Write("Enter n, c, d: ");
			string[] input = Console.ReadLine().Split(' ');
			if (input.Length < 3 ||
				!Int32.TryParse(input[0], out n) ||
				!Int32.TryParse(input[1], out c) ||
				!Int32.TryParse(input[2], out d))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			Console.WriteLine("2. Дан целочисленный вектор A(n). Найти номер последнего максимального элемента среди элементов, лежащих в диапазоне[c, d] и расположенных до первого четного элемента.\n");

			int[] a = new int[n];
			enter_array(a);
			show_array(a);

			int max_i = -1;
			int max_v = -100;
			
			for (int i = 0; i < n; ++i)
			{
				if (a[i] % 2 == 0) break; // до первого четного элемента

				if (a[i] >= c && a[i] <= d && a[i] > max_v)
				{
					max_v = a[i];
					max_i = i;
				}
			}

			if (max_i == -1)
			{
				Console.WriteLine("No elements");
			}
			else
			{
				Console.WriteLine("Last max index between " + c + " and " + d + ": " + max_i);
			}
		}
	}
}
