﻿using System;

namespace lab3
{
	class Program
	{
		static double f(double x)
		{
			if (x < 1)
			{
				return -Math.Abs(x) + x + Math.Pow(x - 1, 3);
			}
			else
			if (x > 1)
			{
				double sum = 0;
				for (int i = 1; i <= 12; ++i)
				{
					sum += (Math.Pow(i, 2) + 1) / (Math.Pow(i, 3) + 3 * i + 1);
				}
				return Math.Sqrt(5 + x) - sum;
			}
			else
			{
				return (Math.Pow(x, 2) + 4 - Math.Sin(Math.Pow(x, 2))) / x;
			}
		}

		static void Main(string[] args)
		{
			double n;
			double k;
			double h;
			Console.Write("Enter n, k, h: ");
			string[] input = Console.ReadLine().Split(' ');
			if (input.Length < 3 || 
				!Double.TryParse(input[0], out n) || 
				!Double.TryParse(input[1], out k) ||
				!Double.TryParse(input[2], out h))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			Console.WriteLine("--------------------------------------");
			Console.WriteLine("| x     | f(x)                       |");
			Console.WriteLine("--------------------------------------");
			for (double x = n; x <= k; x += h)
			{
				Console.WriteLine("| " + x + "\t| " + f(x) + "\t");
			}
			Console.WriteLine("--------------------------------------");
		}
	}
}
