﻿using System;

namespace lab4_1
{
	class Program
	{
		static void fill_array(int[] a)
		{
			Random rand = new Random();
			for (int i = 0; i < a.Length; ++i)
			{
				a[i] = rand.Next(-99, 99);
			}
		}

		static void show_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write(a[i] + "\t");
			}
			Console.WriteLine();
		}

		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;

			int n;
			Console.Write("Enter n: ");
			string input = Console.ReadLine();
			if (!Int32.TryParse(input, out n))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			Console.WriteLine("1. Дан вектор A(2n). Все четные числа, стоящие за максимальным элементом, домножить на минимальный элемент.\n");

			int[] a = new int[2 * n];
			fill_array(a);
			show_array(a);

			int min_v = a[0];
			int max_v = a[0];
			int max_i = 0;
			for (int i = 0; i < a.Length; ++i)
			{
				if (a[i] < min_v)
				{
					min_v = a[i];
				}
				if (a[i] > max_v)
				{
					max_v = a[i];
					max_i = i;
				}
			}

			for (int i = max_i + 1; i < a.Length; ++i)
			{
				if (a[i] % 2 == 0) a[i] *= min_v;
			}

			show_array(a);
		}
	}
}
