﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace kr5_2
{
	class Program
	{
		static void Main(string[] args)
		{
			// 2) Последнее слово строки поставить после первого.

			Console.OutputEncoding = System.Text.Encoding.UTF8;

			string text = 
				"Смеркалось.  \n" +
				"В пять часов утра еще было совсем темно.\n" +
				"Войска центра, резервов и правый фланг Багратиона стояли еще неподвижно.\n" +
				"Но на левом фланге колонны пехоты, кавалерии и артиллерии, долженствовавшие первые спуститься с высот.\n" +
				"Для того чтобы атаковать французский правый фланг и отбросить его, по диспозиции, в Богемские горы, уже зашевелились и начали подниматься с своих ночлегов.\n" +
				"Дым от костров, в которые бросали все лишнее, ел глаза.\n";

			Console.WriteLine("Source text:\n" + text + "\n");

			// split text into sentences and words
			List<List<string>> sentences = 
				Regex.Split(text, @"\. *\n")
				.Select(
					(sentence) => sentence
						.Trim()
						.Split(' ')
						.Select((word) => word.Trim())
						.ToList()
				).ToList();

			string new_text = "";

			// make new text
			foreach (List<string> sentence in sentences)
			{
				// insert last word to second position
				if (sentence.Count >= 3)
				{
					sentence.Insert(1, sentence.ElementAt(sentence.Count - 1));
					sentence.RemoveAt(sentence.Count - 1);
				}

				// join sentence
				string s = String.Join(" ", sentence);
				new_text += (s.Length == 0) ? "" : s + ".\n";
			}

			Console.WriteLine("\nLast word inserted into 2nd position:\n" + new_text + "\n");
		}
	}
}
