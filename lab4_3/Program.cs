﻿using System;

namespace lab4_3
{
	class Program
	{
		static void enter_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write("Enter a[" + i + "]: ");
				string input = Console.ReadLine();
				while (!Int32.TryParse(input, out a[i]))
				{
					Console.WriteLine("Wrong input");
				}
			}
		}

		static void show_array(int[] a)
		{
			for (int i = 0; i < a.Length; ++i)
			{
				Console.Write(a[i] + "\t");
			}
			Console.WriteLine();
		}

		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.UTF8;

			int n;
			Console.Write("Enter n: ");
			string input = Console.ReadLine();
			if (!Int32.TryParse(input, out n))
			{
				Console.WriteLine("Wrong input");
				return;
			}

			Console.WriteLine("3. Найти сумму элементов, расположенных между первым и последним максимальными элементами.\n");

			int[] a = new int[n];
			enter_array(a);
			show_array(a);

			int first_max_v = a[0];
			int first_max_i = 0;
			int last_max_v  = a[0];
			int last_max_i  = 0;
			
			// last max
			for (int i = 0; i < n; ++i)
			{
				if (a[i] >= last_max_v)
				{
					last_max_v = a[i];
					last_max_i = i;
				}
			}

			// first max
			for (int i = n - 1; i >= 0; --i)
			{
				if (a[i] >= last_max_v)
				{
					first_max_v = a[i];
					first_max_i = i;
				}
			}

			if (first_max_i == last_max_i)
			{
				Console.WriteLine("The array has the only one max element");
			}
			else
			{
				int sum = 0;
				for (int i = first_max_i + 1; i < last_max_i; ++i)
				{
					sum += a[i];
				}
				Console.WriteLine("Sum between first and last max elements: " + sum);
			}
		}
	}
}
