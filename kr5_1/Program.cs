﻿using System;
using System.Text.RegularExpressions;

namespace kr5_1
{
	class Program
	{
		static void Main(string[] args)
		{
			// 1) Подсчитать количество символов '.' , стоящих перед пробелом, и заменить каждую пару символов 'ST' на  символ 'P'.

			Console.OutputEncoding = System.Text.Encoding.UTF8;

			string text = "ST В пять часов утра еще ST было совсем темно. ST Войска центра, резервов и правый фланг Багратиона стояли еще неподвижно. Но на левом фланге колонны пехоты, кавалерии и артиллерии, долженствовавшие первые спуститься с высот, для того чтобы атаковать французский правый фланг и отбросить его, по диспозиции, в Богемские горы, уже зашевелились и начали подниматься с своих ночлегов. Дым от костров, в которые бросали все лишнее, ел глаза.";

			Console.WriteLine("Source text:\n" + text + "\n");

			Console.WriteLine("Count of \". \": " + Regex.Matches(text, @"\. ").Count);

			Console.WriteLine("\nST replaced to P:\n" + text.Replace("ST", "P"));
		}
	}
}
